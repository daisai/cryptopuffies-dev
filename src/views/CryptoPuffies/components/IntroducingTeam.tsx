import React from 'react'
import styled from 'styled-components'
import Slider from "react-slick";
import { Flex, Text } from 'penguinfinance-uikit2'
import SvgIcon from 'components/SvgIcon';
import useWindowSize from 'hooks/useWindowSize'

const teamMembers = [
  { id: 1, name: 'Pete Framistar', role: 'Project Lead', image: 'member1.png' },
  { id: 2, name: 'Pete Framistar', role: 'Project Lead', image: 'member2.png' },
  { id: 3, name: 'Pete Framistar', role: 'Project Lead', image: 'member3.png' },
  { id: 4, name: 'Pete Framistar', role: 'Project Lead', image: 'member4.png' },
  { id: 5, name: 'Pete Framistar', role: 'Project Lead', image: 'member5.png' },
  { id: 6, name: 'Pete Framistar', role: 'Project Lead', image: 'member6.png' },
]



const IntroducingTeam = () => {
  const windowSize = useWindowSize();

  const settings = {
    // className: "center",
    // centerMode: true,
    infinite: true,
    // centerPadding: "60px",
    slidesToShow: 2,
    slidesToScroll: 1,
    speed: 500,
    variableWidth: true,
    nextArrow: (
      <NextArrow>
        <SvgIcon src={`${process.env.PUBLIC_URL}/images/cryptopuffies/team/arrow-left.svg`} width='20px' height='20px' />
      </NextArrow>
    )
  };

  return (
    <>
      <IntroductionContainer alignItems="center">
        <IntroducingTeamHeader>
          <IntroducingTeamHeaderTitle>
            <FirstSentence>{`Introducing the `}</FirstSentence>
            <SecondSentence>{`Team `}</SecondSentence>
          </IntroducingTeamHeaderTitle>
          <IntroducingTeamHeaderDescription>
            <p>
              {`Our Team is composed of multiple crypto investors, crypto-enthusiasts, and award winning developers. What
            brings us together is our passion for crypto, and NFT’s and most of all… `}
            </p>
            <p>
              {`Decentralization. We as a community value the power of having full control of the value of our funds and
            intend to help build upon this amazing decentralized ecosystem. `}
            </p>
          </IntroducingTeamHeaderDescription>
        </IntroducingTeamHeader>
        {windowSize.width >= 1400 &&
          <SubListContainer>
            {teamMembers
              .filter((row) => row.id <= 2)
              .map((member) => (
                <MemberCard key={member.id} memberId={member.id}>
                  <MemberImage
                    src={`${process.env.PUBLIC_URL}/images/cryptopuffies/team/${member.image}`}
                    alt="member image"
                  />
                  <MemberInfo>
                    <MemberName>{member.name}</MemberName>
                    <MemberRole>{member.role}</MemberRole>
                  </MemberInfo>
                </MemberCard>
              ))}
          </SubListContainer>
        }
      </IntroductionContainer>
      {windowSize.width < 968 ? 
        <SliderContainer flexDirection='column' mt='40px'>
          <Slider {...settings}>
            {teamMembers
              .filter((row) => row.id <= 4)
              .map((member) => (
                <MemberCard key={member.id} memberId={2}>
                  <Flex flexDirection='column' alignItems='center'>
                    <MemberImage
                      src={`${process.env.PUBLIC_URL}/images/cryptopuffies/team/${member.image}`}
                      alt="member image"
                    />
                    <MemberInfo>
                      <MemberName>{member.name}</MemberName>
                      <MemberRole>{member.role}</MemberRole>
                    </MemberInfo>
                  </Flex>
                </MemberCard>
              ))}
          </Slider>
        </SliderContainer>
        : <>
          {windowSize.width >= 1400 ?
            <ListContainer>
              {teamMembers
                .filter((row) => row.id > 2)
                .map((member) => (
                  <MemberCard key={member.id} memberId={member.id}>
                    <MemberImage
                      src={`${process.env.PUBLIC_URL}/images/cryptopuffies/team/${member.image}`}
                      alt="member image"
                    />
                    <MemberInfo>
                      <MemberName>{member.name}</MemberName>
                      <MemberRole>{member.role}</MemberRole>
                    </MemberInfo>
                  </MemberCard>
                ))}
            </ListContainer>
            :
            <Flex flexDirection='column' alignItems='center' mt='100px'>
              <MembersContainer colCount={4}>
                {teamMembers
                  .filter(row => row.id <= 4)
                  .map((member) => (
                  <MemberCard key={member.id} memberId={member.id + 1}>
                    <MemberImage
                      src={`${process.env.PUBLIC_URL}/images/cryptopuffies/team/${member.image}`}
                      alt="member image"
                    />
                    <MemberInfo>
                      <MemberName>{member.name}</MemberName>
                      <MemberRole>{member.role}</MemberRole>
                    </MemberInfo>
                  </MemberCard>
                ))}
              </MembersContainer>
              <MembersContainer colCount={2}>
                {teamMembers
                  .filter(row => row.id > 4)
                  .map((member) => (
                  <MemberCard key={member.id} memberId={member.id}>
                    <MemberImage
                      src={`${process.env.PUBLIC_URL}/images/cryptopuffies/team/${member.image}`}
                      alt="member image"
                    />
                    <MemberInfo>
                      <MemberName>{member.name}</MemberName>
                      <MemberRole>{member.role}</MemberRole>
                    </MemberInfo>
                  </MemberCard>
                ))}
              </MembersContainer>
            </Flex>
          }
        </>
      }
    </>
  )
}

const NextArrow = styled.div`
  div {
    position: absolute;
    margin-top: -180px;
    margin-left: -20px;
  }
`;

const SliderContainer = styled(Flex)`
  .slick-list {
    max-height: 214px;
  }
`;

const IntroductionContainer = styled(Flex)`
  flex-direction: column;
  @media (min-width: 768px) {
    flex-direction: column;
  }
  @media (min-width: 1200px) {
    flex-direction: column;
  }
  @media (min-width: 1400px) {
    flex-direction: row;
  }
`;

// header
const IntroducingTeamHeader = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;

  @media (min-width: 968px) {
    width: 80%;
  }
  @media (min-width: 1400px) {
    width: 50%;
    padding-right: 24px;
  }
`
const IntroducingTeamHeaderTitle = styled(Flex)`
  white-space: break-spaces;
  flex-wrap: wrap;
`
const FirstSentence = styled.span`
  font-family: 'Gotham-bold';
  font-style: normal;
  font-weight: bold;
  font-size: 26px;
  line-height: 42px;
  color: #050021;

  @media (min-width: 768px) {
    font-size: 26px;
  }
  @media (min-width: 968px) {
    font-size: 42px;
    line-height: 72px;
  }
  @media (min-width: 1200px) {
    font-size: 52px;
  }
`
const SecondSentence = styled.span`
  font-family: 'Gotham-bold';
  font-style: normal;
  font-weight: bold;
  font-size: 26px;
  line-height: 42px;
  background: linear-gradient(to right, #9d7eff 0%, #ffc5e3 100%);
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;

  @media (min-width: 768px) {
    font-size: 26px;
  }
  @media (min-width: 968px) {
    font-size: 42px;
    line-height: 72px;
  }
  @media (min-width: 1200px) {
    font-size: 52px;
  }
`

const IntroducingTeamHeaderDescription = styled.div`
  padding-right: 0px;
  P {
    margin-top: 8px;
    font-family: 'Gotham';
    font-style: normal;
    font-weight: normal;
    font-size: 18px;
    line-height: 27px;
    text-align: center;

    @media (min-width: 968px) {
      text-align: left;
    }
  }
  @media (min-width: 1200px) {
    padding-right: 30px;
  }
  @media (min-width: 1400px) {
    padding-right: 30px;
  }
`

// sub list
const SubListContainer = styled.div`
  display: grid;
  grid-gap: 30px;
  grid-template-columns: repeat(2, 1fr);
  padding: 16px 0;
  width: 50%;

  ${({ theme }) => theme.mediaQueries.lg} {
    grid-template-columns: repeat(2, 1fr);
  }
`

// list
const ListContainer = styled.div`
  margin-top: 16px;
  display: grid;
  grid-gap: 30px;
  grid-template-columns: repeat(2, 1fr);
  padding: 16px 0;

  ${({ theme }) => theme.mediaQueries.sm} {
    grid-gap: 24px;
    grid-template-columns: repeat(4, 1fr);
    padding: 24px 0;
  }

  ${({ theme }) => theme.mediaQueries.md} {
    grid-template-columns: repeat(4, 1fr);
  }

  ${({ theme }) => theme.mediaQueries.lg} {
    grid-template-columns: repeat(4, 1fr);
  }
`

const MembersContainer = styled.div<{ colCount: number }>`
  display: grid;
  grid-gap: 30px;
  grid-template-columns: repeat(2, 1fr);
  padding: 16px 0;

  ${({ theme }) => theme.mediaQueries.sm} {
    grid-gap: 24px;
    grid-template-columns: repeat(8, 1fr);
    padding: 24px 0;

    div {
      grid-column: span 2;

      &:last-child:nth-child(2) {
        grid-column-end: ${({ colCount }) => colCount === 2 && -3};
      }
      
      &:first-child {
        grid-column-end: ${({ colCount }) => colCount === 2 && 5};
      }
    }
  }
`;

const MemberCard = styled.div<{ memberId?: number }>`
  background: #f5f5ff;
  border-radius: 16px;
  padding: 16px;
  transform: ${({ memberId }) => memberId % 2 === 1 && 'translateY(-30px)'};
  width: 214px;
  min-width: 214px;
  margin-right: 16px;
  height: 214px;
  max-width: 214px;

  @media (min-width: 968px) {
    width: 100%;
    height: unset;
    max-width: 285px;
    margin-right: 0;
  }
`
const MemberImage = styled.img`
  height: 100px;
  margin-bottom: 24px;
  // z-index: -1;

  @media (min-width: 968px) {
    width: 100%;
    height: unset;
    margin-bottom: 0;
  }
`
const MemberInfo = styled.div`
  text-align: center;
  background: rgba(255, 255, 255, 0.8);
  backdrop-filter: blur(37px);
  padding: 8px;
  width: 100%;

  @media (min-width: 968px) {
    width: unset;
  }
`
const MemberName = styled(Text)`
  font-family: 'Gotham';
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 27px;
  color: #050021;
  margin: auto;
`
const MemberRole = styled(Text)`
  font-family: 'Gotham';
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 18px;
  color: #050021;
  margin: auto;
  margin-top: 2px;
`

export default IntroducingTeam
