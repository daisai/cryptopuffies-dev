import { configureStore } from '@reduxjs/toolkit'
import { useDispatch } from 'react-redux'
import globalReducer from './global'

const store = configureStore({
  devTools: process.env.NODE_ENV !== 'production',
  reducer: {
    global: globalReducer,
  },
})

export type AppDispatch = typeof store.dispatch
export const useAppDispatch = () => useDispatch<AppDispatch>()

export default store
