import { useSelector } from 'react-redux'

import { State, GlobalState } from './types'

export const useFetchPublicData = () => {
  return null;
}

// Global
export const useGlobal = () => {
  const { wrongNetworkGuideModalOpened }: GlobalState = useSelector((state: State) => state.global)

  return { wrongNetworkGuideModalOpened }
}
